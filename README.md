# Products Phing Tasks

Generic phing tasks for the best it products.

## Key Facts

Customer: [best it](http://bestit-online.de/)

Product Owner / Project Manager: [Melanie Nowaczyk](mailto:melanie.nowaczyk@bestit-online.de)

Teach Lead: [Martin Knoop](mailto:martin.knoop@bestit-online.de)

GIT Flow: [Feature Branch Flow](https://www.atlassian.com/git/tutorials/comparing-workflows#feature-branch-workflow)

System: [Phing](https://www.phing.info/)

## Development Setup

 Checkout the project and install the dependencies. An local environment is not required.

 ```shell
 $ composer install
 ```

### Technical requirements

* PHP 7.0

## (Software) Architecture

 The following components are included in this project:

- Multiple phing XML and property files for the individual instances
- Custom PHP Phing tasks to handle specific tasks

You can import this files in the product project to handle the generic build process

#### Tasks
| Task        | Description | Configuration |
| ------------------|-----------------|----------|
| shopware:install:users| Install the configured users | Every user needs to be configured in the given property files, this allows us to install environment specific users|

##### Support for the new plugin structure
If the product is written in the new plugin structure you need to set the plugin.legacy property to false and change the plugin.sw.dir property to ${dir.html}/custom/project.

##### Support for an flat repository structure
If the product repository contains only one composer.json file, we need to enable the flat structure mode.
This mode allows us to differentiate between plugin files and build/dev files in the repository via a fileset.
Only the files that are included in this set will be copied into the plugin artifact and released.
To activate this mode you need to set the property plugin:flat_structure=true.
Additionally you need to declare an fileset with all plugin files. The following example demonstrates this:

  ```xml
    <fileset id="plugin-files" dir="${project.basedir}">
        <include name="Bundle/**"/>
        <include name="Controllers/**"/>
        <include name="Files/**"/>
        <include name="Models/**"/>
        <include name="Resources/**"/>
        <include name="Snippets/**"/>
        <include name="Subscriber/**"/>
        <include name="Views/**"/>
        <include name="Plugin.php"/>
        <include name="composer.*"/>
        <include name="README.md"/>
        <include name="plugin.*"/>
    </fileset>
  ```

##### Adding new users
To add an new user you need to append the user identifier to the existing property shopware:user and you need to configure the specific credentials properties.
In the following example an additional user with the name test will be added to the vagrant environment. (config/vagrant.properties)

  ```shell
  shopware.users=demo,test
  user.test.name=test
  user.test.password=test
  user.test.mail=test@example.com
  ```

#### Setting the plugin config
To set the plugin config in the build.xml file you need to implement the task plugin:configure and execute your tasks there. To help you with this configuration the package contains the following task:
```xml
  <phingcall target="plugin:config:set">
      <property name="key" value="foobar"/>
      <property name="value" value="false"/>
      <property name="shopId" value="3" />
      <property name="type" value="1" />
  </phingcall>
```
Because the values are serialized you need to declare the type of the value. The follwoing types are supported:
| Type        | Key |
| ------------|-----|
| Boolean     | 1   |
| Integer     | 2   |
| Float       | 3   |
| String      | 4   |
| Default     | 5   |


#### Integrated Custom Tasks
The following custom tasks are implemented:

| Task        | Description | Attributes |
| ------------------|-----------------|----------|
| pluginReleasePrepare| prepare the project plugin files for the release  | root: root folder for the plugin source, replacedAuthor: replace all authors in the files with the configured author, prependFile: prepend a doc block at the start of an php file, excludeFolders: folders that should be excluded, delimiter "," |

To use the custom task in your project you need to create a new target that executes this task e.g.:
```xml
    <target name="plugin:prepare:file">
        <pluginReleasePrepare root="${project.basedir}/src/${plugin.name}" replacedAuthor="best it &lt;info@bestit-online.de&gt;" prependFile="${dir.build}/prepend.txt" excludeFolders="Components"/>
    </target>
```

## Testing

This project uses the PHP Unit framework.
Test can be found under ./tests

To run the tests use PHP Unit:

```shell
$ vendor/bin/phpunit
```

## Build & Deployment

No separate build and deployment process is implemented.
