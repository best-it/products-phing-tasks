<?php

namespace Bestit\PhingTasks\Tasks\Fixtures;

/**
 * Test class
 *
 * @package Bestit\PhingTasks\Tasks\Fixtures
 * @author foobar <test>
 */
class Test
{
    /**
     * @var string
     */
    private $prop1;

    /**
     * Test constructor.
     *
     * @param string $prop1
     */
    public function __construct(string $prop1)
    {
        $this->prop1 = $prop1;
    }

    /**
     * Get Prop1
     *
     * @return string
     */
    public function getProp1(): string
    {
        return $this->prop1;
    }

    /**
     * Set Prop1
     *
     * @param string $prop1
     */
    public function setProp1($prop1)
    {
        $this->prop1 = $prop1;
    }
}