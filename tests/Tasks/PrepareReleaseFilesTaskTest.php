<?php

use Bestit\PhingTasks\Tasks\PrepareReleaseFilesTask;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use org\bovigo\vfs\vfsStreamFile;
use PHPUnit\Framework\TestCase;

/**
 * Tests for the custom task
 *
 * @author Martin Knoop <martin.knoop@bestit-online.de>
 */
class PrepareReleaseFilesTaskTest extends TestCase
{
    /**
     * @var vfsStreamDirectory
     */
    private $root;

    /**
     * @var string
     */
    private $rootPath;

    /**
     * Set up the test
     *
     * @return void
     */
    public function setUp()
    {
        $fixtureContent = file_get_contents(__DIR__ . '/Fixtures/Test.php');

        $structure = [
            'foobar' => [
                'bar' => [
                    'foobar.php' => $fixtureContent
                ]
            ],
            'foobarbar' => [
                'foo' => [
                    'bar' => [
                        'foobar.php' => $fixtureContent,
                        'foobar.xml' => '<xml/>'
                    ]
                ],
                'bar' => [
                    'test.php' => $fixtureContent
                ],
                'bar.php' => $fixtureContent
            ],
            'barfoo' => [
                'foobar.php' => $fixtureContent
            ],
            'barfoo1' => [
                'foobar1.php' => $fixtureContent
            ],
            'vendor' => [
                'barz' => [
                    'bar.php' => $fixtureContent
                ]
            ]
        ];

        vfsStream::setup();
        $this->root = vfsStream::create($structure);
        $this->rootPath = vfsStream::url($this->root->getName());
    }

    /**
     * Test that file swill be modified
     *
     * @return void
     */
    public function testThatFilesWillBeModified()
    {
        $task = new  PrepareReleaseFilesTask();
        $task->setRoot($this->rootPath);
        $task->setReplacedAuthor('best it');
        $task->setPrependFile(__DIR__ . '/Fixtures/append.txt');
        $task->setExcludeFolders('barfoo, barfoo1');

        $task->main();
        static::assertSame(
            json_decode(file_get_contents(__DIR__ . '/Fixtures/changed_files.json'), true),
            $this->createStructure([], $this->root->getChildren())
        );
    }

    /**
     * Create structure from virtual dir
     *
     * @param array$array
     * @param array $children
     *
     * @return array
     */
    private function createStructure(array $array, array $children)
    {
        foreach ($children as $child) {
            if ($child instanceof vfsStreamDirectory) {
                $array[$child->getName()][] = $this->createStructure([], $child->getChildren());
            } elseif ($child instanceof vfsStreamFile) {
                $content = $child->getContent();
                $array[$child->getName()] = $content;
            }
        }

        return $array;
    }
}
