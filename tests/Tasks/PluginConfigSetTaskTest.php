<?php

declare(strict_types = 1);

use Doctrine\DBAL\Connection;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Bestit\PhingTasks\Tasks\PluginConfigSetTask;

/**
 * The tests for PluginConfigSetTask
 *
 * @author Martin Knoop <info@bestit-online.de>
 */
class PluginConfigSetTaskTest extends TestCase
{
    /**
     * Provider for the task test
     *
     * @return array
     */
    public function valueProvider(): array
    {
        return [
            [true, PluginConfigSetTask::BOOLEAN_TYPE, 'b:1;'],
            [false, PluginConfigSetTask::BOOLEAN_TYPE, 'b:0;'],
            ['foobar', PluginConfigSetTask::STRING_TYPE, 's:6:"foobar";'],
            [1, PluginConfigSetTask::INTEGER_TYPE, 'i:1;'],
            [12.5, PluginConfigSetTask::FLOAT_TYPE, 'd:12.5;'],
            ['foobar', PluginConfigSetTask::DEFAULT_TYPE, 's:6:"foobar";']
        ];
    }

    /**
     * Test that the config will be set
     *
     * @param mixed $value
     * @param int $type
     * @param string $serializedValue
     *
     * @dataProvider valueProvider
     *
     * @return void
     */
    public function testThatTheConfigWillBeSet($value, int $type, string $serializedValue)
    {
        /** @var PluginConfigSetTask|MockObject $task */
        $task = $this->createPartialMock(PluginConfigSetTask::class, ['getDatabase']);
        $task->setPluginName($pluginName = 'pluginName');
        $task->setKey($key = 'key');
        $task->setValue($value);
        $task->setType($type);
        $task->setShopId($shopId = 1);

        $shopSql = <<<sql
SELECT scs.id as defaultId, (SELECT scs1.id from s_core_shops scs1 WHERE scs1.id = :id) as shopId
FROM s_core_shops scs
WHERE `default` = 1
sql;

        $pluginSql = <<<sql
SELECT 1 FROM s_core_plugins WHERE name LIKE :name
sql;

        $database = $this->createMock(Connection::class);
        $database
            ->expects(self::once())
            ->method('fetchAssoc')
            ->with($shopSql)
            ->willReturn(['defaultId' => 1, 'shopId' => $shopId]);

        $database
            ->expects(self::once())
            ->method('fetchColumn')
            ->with($pluginSql, ['name' => $pluginName])
            ->willReturn(['defaultId' => 1, 'shopId' => $shopId]);

        $sql = <<<sql
REPLACE INTO s_core_config_values (element_id, value, shop_id)
SELECT scce.id as element_id, :value as value, :shopId as shop_id
FROM s_core_config_elements scce
       INNER JOIN s_core_config_forms sccf ON scce.form_id = sccf.id
       LEFT JOIN s_core_plugins scp ON scp.id = sccf.plugin_id
WHERE scp.name LIKE :pluginName
  AND scce.name LIKE :configKey
sql;

        $params = [
            'value' => $serializedValue,
            'shopId' => $shopId,
            'pluginName' => $pluginName,
            'configKey' => $key
        ];

        $database
            ->expects(self::once())
            ->method('executeQuery')
            ->with($sql, $params);

        $task->method('getDatabase')->willReturn($database);
        $task->main();
    }
}
