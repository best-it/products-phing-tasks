<?php

namespace  Bestit\PhingTasks\Tasks;

use BuildException;
use Generator;
use RecursiveCallbackFilterIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RegexIterator;
use SplFileInfo;
use Task;

/**
 * Custom phing task to prepare a given folder for an release
 *
 * @package Bestit\PhingTasks\Tasks
 * @author Martin Knoop <martin.knoop@bestit-online.de>
 */
class PrepareReleaseFilesTask extends Task
{
    /**
     * The php open tag
     *
     * @var string
     */
    const PHP_OPEN_TAG = '<?php';

    /**
     * @var string|null
     */
    private $prependFile;

    /**
     * @var string|null
     */
    private $replacedAuthor;

    /**
     * @var string
     */
    private $root;

    /**
     * @var array
     */
    private $excludeFolders = ['vendor'];

    /**
     * Set the prepended file
     *
     * @param null|string $prependFile
     */
    public function setPrependFile($prependFile)
    {
        $this->prependFile = $prependFile;
    }

    /**
     * Set the replace author
     *
     * @param null|string $replacedAuthor
     */
    public function setReplacedAuthor($replacedAuthor)
    {
        $this->replacedAuthor = $replacedAuthor;
    }

    /**
     * Set the root folder
     *
     * @param string $root
     */
    public function setRoot($root)
    {
        $this->root = $root;
    }

    /**
     * Add the excluded folder
     *
     * @param string $excludeFolders
     */
    public function setExcludeFolders($excludeFolders)
    {
        $this->excludeFolders = array_map('trim', array_merge($this->excludeFolders, explode(',', $excludeFolders)));
    }

    /**
     * Main method to run the task
     *
     * @return void
     *
     * @throws BuildException
     */
    public function main()
    {
        if ($this->prependFile === null && $this->replacedAuthor === null) {
            throw new BuildException('Invalid options given');
        }

        if ($this->prependFile && !is_file($this->prependFile)) {
            throw new BuildException('Invalid append file given');
        }

        $prependContent = $this->prependFile ? file_get_contents($this->prependFile) : null;
        $author = $this->buildAuthorTag();

        foreach ($this->provideFiles($this->root) as $file) {
            $this->processFile($file, $prependContent, $author);
        }
    }

    /**
     * Process given file
     *
     * @param string $filePath
     *
     * @param null|string $prependContent
     * @param null|string $replacedAuthor
     *
     * @return void
     */
    private function processFile($filePath, $prependContent = null, $replacedAuthor = null)
    {
        $content = '';

        $fileContent = file_get_contents($filePath);

        if ($replacedAuthor) {
            $pattern = '/[\n\r].*@author\s*([^\n\r]*)/';
            $fileContent = preg_replace($pattern, $replacedAuthor,  $fileContent);
        }

        if ($prependContent) {
            $content .= self::PHP_OPEN_TAG . PHP_EOL . PHP_EOL . $prependContent;
            $fileContent = str_replace(self::PHP_OPEN_TAG, '', $fileContent);
        }

        $content .= $fileContent;

        if ($content === '') {
            $message = 'Error at processing file empty file given, file ' . $filePath;
            throw new BuildException($message);
        }

        if (file_put_contents($filePath, $content) === false) {
            $message = 'Error at writing file, safety abort' ;
            throw new BuildException($message);
        }
    }

    /**
     * Build author tag
     *
     * @return null|string
     */
    private function buildAuthorTag()
    {
        $author = null;
        if ($this->replacedAuthor) {
            $author = PHP_EOL . ' * @author ' . $this->replacedAuthor;
        }

        return $author;
    }

    /**
     * Provide all php files in the given folder, returns an generator because of large file set
     *
     * @param string $path
     *
     * @return Generator|string
     */
    private function provideFiles($path)
    {
        $filter = function (SplFileInfo $file, $key, RecursiveDirectoryIterator $iterator) {
            return ($iterator->hasChildren() && !in_array($file->getFilename(), $this->excludeFolders)) ? true :  $file->isFile();
        };

        // Iterate over all files recursive
        $directoryIterator = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS);
        // Exclude blacklisted dirs
        $recursiveIterator = new RecursiveIteratorIterator(new RecursiveCallbackFilterIterator($directoryIterator, $filter));
        // Only return php files
        $filteredIterator = new RegexIterator($recursiveIterator, '/^.+\.php$/i', RegexIterator::GET_MATCH);

        foreach ($filteredIterator as $pathname => $fileInfo) {
            yield $pathname;
        }
    }
}