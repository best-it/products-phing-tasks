<?php

declare(strict_types = 1);

namespace Bestit\PhingTasks\Tasks;

use BuildException;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Exception;
use Task;

/**
 * Custom phing task set the plugin config
 *
 * @package Bestit\PhingTasks\Tasks
 * @author Martin Knoop <martin.knoop@bestit-online.de>
 */
class PluginConfigSetTask extends Task
{
    /**
     * Key for the default type
     */
    const DEFAULT_TYPE = 0;

    /**
     * Key for the boolean type
     */
    const BOOLEAN_TYPE = 1;

    /**
     * Key for the integer type
     */
    const INTEGER_TYPE = 2;

    /**
     * Key for the float type
     */
    const FLOAT_TYPE = 3;

    /**
     * Key for the string type
     */
    const STRING_TYPE = 4;

    /**
     * The config key
     *
     * @var string|null
     */
    private $key;

    /**
     * The config value
     *
     * @var string|null
     */
    private $value;

    /**
     * The type of the value
     *
     * @var int|null
     */
    private $type = self::DEFAULT_TYPE;

    /**
     * The plugin name
     *
     * @var string|null
     */
    private $pluginName;

    /**
     * The shop id
     *
     * @var int|null
     */
    private $shopId;

    /**
     * @var string|null
     */
    private $databaseHost;

    /**
     * @var string|null
     */
    private $databaseName;

    /**
     * @var string|null
     */
    private $databaseUser;

    /**
     * @var string|null
     */
    private $databasePassword;

    /**
     * @var int|null
     */
    private $databasePort;

    /**
     * The database adapter
     *
     * @var Connection|null
     */
    private $connection;

    /**
     * The setter for key
     *
     * @param string|null $key
     *
     * @return PluginConfigSetTask
     */
    public function setKey($key): PluginConfigSetTask
    {
        $this->key = $key;
        return $this;
    }

    /**
     * The setter for value
     *
     * @param string|null $value
     *
     * @return PluginConfigSetTask
     */
    public function setValue($value): PluginConfigSetTask
    {
        $this->value = $value;
        return $this;
    }

    /**
     * The setter for pluginName
     *
     * @param string|null $pluginName
     *
     * @return PluginConfigSetTask
     */
    public function setPluginName($pluginName): PluginConfigSetTask
    {
        $this->pluginName = $pluginName;
        return $this;
    }

    /**
     * The setter for type
     *
     * @param int|null $type
     *
     * @return PluginConfigSetTask
     */
    public function setType($type): PluginConfigSetTask
    {
        $this->type = $type;
        return $this;
    }

    /**
     * The setter for shopId
     *
     * @param int|null $shopId
     *
     * @return PluginConfigSetTask
     */
    public function setShopId($shopId): PluginConfigSetTask
    {
        $this->shopId = $shopId;
        return $this;
    }

    /**
     * The setter for databaseHost
     *
     * @param string|null $databaseHost
     *
     * @return PluginConfigSetTask
     */
    public function setDatabaseHost($databaseHost): PluginConfigSetTask
    {
        $this->databaseHost = $databaseHost;
        return $this;
    }

    /**
     * The setter for databasePort
     *
     * @param int|null $databasePort
     *
     * @return PluginConfigSetTask
     */
    public function setDatabasePort($databasePort): PluginConfigSetTask
    {
        $this->databasePort = $databasePort;
        return $this;
    }

    /**
     * The setter for databaseName
     *
     * @param string|null $databaseName
     *
     * @return PluginConfigSetTask
     */
    public function setDatabaseName($databaseName): PluginConfigSetTask
    {
        $this->databaseName = $databaseName;
        return $this;
    }

    /**
     * The setter for databaseUser
     *
     * @param string|null $databaseUser
     *
     * @return PluginConfigSetTask
     */
    public function setDatabaseUser($databaseUser): PluginConfigSetTask
    {
        $this->databaseUser = $databaseUser;
        return $this;
    }

    /**
     * The setter for databasePassword
     *
     * @param string|null $databasePassword
     *
     * @return PluginConfigSetTask
     */
    public function setDatabasePassword($databasePassword): PluginConfigSetTask
    {
        $this->databasePassword = $databasePassword;
        return $this;
    }

    /**
     * Main method to run the task
     *
     * @return void
     *
     * @throws BuildException
     */
    public function main()
    {
        $shopSql = <<<SQL
SELECT scs.id as defaultId, (SELECT scs1.id from s_core_shops scs1 WHERE scs1.id = :id) as shopId
FROM s_core_shops scs
WHERE `default` = 1
SQL;
        $pluginSql = <<<SQL
SELECT 1 FROM s_core_plugins WHERE name LIKE :name
SQL;
        $shops = $this->getDatabase()->fetchAssoc($shopSql, ['id' => $this->shopId]);

        $shopId = $shops['shopId'] ?? $shops['defaultId'];

        $pluginExists = (bool) $this->getDatabase()->fetchColumn($pluginSql, ['name' => $this->pluginName]);

        if ($shopId !== null && $pluginExists) {
            $sql = <<<SQL
REPLACE INTO s_core_config_values (element_id, value, shop_id)
SELECT scce.id as element_id, :value as value, :shopId as shop_id
FROM s_core_config_elements scce
       INNER JOIN s_core_config_forms sccf ON scce.form_id = sccf.id
       LEFT JOIN s_core_plugins scp ON scp.id = sccf.plugin_id
WHERE scp.name LIKE :pluginName
  AND scce.name LIKE :configKey
SQL;
            $this->getDatabase()->executeQuery($sql,[
                'value' => $this->castValue($this->value),
                'shopId' => $shopId,
                'pluginName' => $this->pluginName,
                'configKey' => $this->key
            ]);
        }
    }

    /**
     * Get the database adapter
     *
     * @throws Exception
     *
     * @return Connection
     */
    public function getDatabase(): Connection
    {
        if ($this->connection === null) {
            $config = new Configuration();

            $connectionParams = [
                'dbname' => $this->databaseName,
                'user' => $this->databaseUser,
                'password' => $this->databasePassword,
                'host' => $this->databaseHost,
                'port' => $this->databasePort ?? 3306,
                'driver' => 'pdo_mysql',
            ];
            $this->connection = DriverManager::getConnection($connectionParams, $config);
        }

        return $this->connection;
    }

    /**
     * Cast the value by the configured type
     *
     * @param mixed $value
     *
     * @return mixed
     */
    private function castValue($value)
    {
        switch ($this->type) {
            case self::BOOLEAN_TYPE:
                $normalizedValue = filter_var($value, FILTER_VALIDATE_BOOLEAN);
                break;
            case self::STRING_TYPE:
                $normalizedValue = (string) $value;
                break;
            case self::INTEGER_TYPE:
                $normalizedValue = (int) $value;
                break;
            case self::FLOAT_TYPE:
                $normalizedValue = (float) $value;
                break;
            case self::DEFAULT_TYPE:
            default:
                $normalizedValue = $value;
        }

        return serialize($normalizedValue);
    }
}